import React, {Component,SearchBar} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';

import {ScrollView, FlatList} from 'react-native-gesture-handler';




export default class HomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      isVisible: true,
      finallist: [],
    };
  }

  async componentWillMount() {



 

    fetch('https://api.github.com/orgs/square/repos', {
      method: 'GET',
  })

      .then((response) => response.json())
      .then((responseJson) => {
    
         
          this.setState({
            finallist :responseJson
          })
    
     
      })
      .catch((error) => {
     

        console.error(error);
      });
    }

 


  
  render() {
    const {finallist} = this.state;

    

    return (
      <ScrollView>
        <Text style={styles.headingtitleText}>Items</Text>
     
        <FlatList
          data={finallist}
          ListHeaderComponent={this.renderHeader}  
          ItemSeparatorComponent={this.renderSeparator} 
          renderItem={({item}) => (
            <TouchableOpacity
            onPress={()=>{
                 this.props.navigation.navigate('RecordsScreen', {
                userName:item,
              });
              }}>

              
           
              <View style={styles.item}>
  

                <Text>Name : {item.name}</Text>
                <Text>Full Name :{item.full_name}</Text>
                <Text>Id: {item.id}</Text>
                <Text>Description:{item.description}</Text>
             

     
              </View>
            </TouchableOpacity>
          )}

        />
      </ScrollView>
    );
            }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#008080',
    padding: 10,
    marginBottom: 10,
    height: 200,
    width: 300,
  },
  button1: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    marginBottom: 10,
    height: 200,
    width: 300,
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  subtitleText: {
    fontSize: 20,
    padding: 40,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  headingtitleText: {
    fontSize: 30,
    padding: 25,
    alignItems:'center',
    alignContent:'center',
    fontWeight: 'bold',
    left:120,
    color: '#000000',
  },
  subheadingtitleText: {
    fontSize: 20,
    padding: 15,
    fontWeight: 'bold',
    color: '#000000',
  },
  item: {
    marginTop: 24,
    padding: 30,
    backgroundColor: 'steelblue',

    borderRadius: 10,
    fontSize: 24,
    marginEnd: 10,
    marginLeft: 10,
  },
  tinyLogo: {
    width: 50,
    height: 50,
    marginBottom: 10,
  },
  textInputStyle: {
    height: 40,
    borderWidth: 1,
    paddingLeft: 20,
    margin: 5,
    borderColor: '#009688',
    backgroundColor: '#FFFFFF',
  },
});
