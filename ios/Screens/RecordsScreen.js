import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Dimensions,
  Button,
 Image,
Alert
  
} from 'react-native';
import {ScrollView, FlatList} from 'react-native-gesture-handler';




export default class RecordsScreen extends Component {
  constructor() {
    super();
   
    this.state = {
      isVisible: false,
      recordList: {},
      pdf : false,
      image :false,
    };
  }

  async componentWillMount() {
    console.log(this.props.route.params.userName)
  }

  toggleModal = () => {
    this.setState({isVisible: false});
  };
  _onPressPrescription(item) {

  
    if(item.prescription == "na.jpg")
    {
      Alert.alert(
        "Message",
        "No Prescription to show",
        [
          
          { text: "OK", onPress: () => 
          {
            console.log("OK Pressed")
          

        } }
        ],
        { cancelable: false }
      );
    }else{
    this.setState({
      source: {uri: 'https://drtop.in/' + item.prescription},
      isVisible: true,
      pdf:true,
      image:false,
    });
  }
  }
  _onPressReceipt(item) {
 
    this.setState({
      source: {uri: 'https://drtop.in/' + item.receipt},
      isVisible: true,
      
    });
  }
  _onPressLaborder(item) {
    if(item.labOrder == "na.jpg")
    {
      Alert.alert(
        "Message",
        "No Lab Order to show",
        [
          
          { text: "OK", onPress: () => 
          {
            console.log("OK Pressed")
          

        } }
        ],
        { cancelable: false }
      );
    }else{
    this.setState({
      source: {uri: 'https://drtop.in/' + item.labOrder},
      isVisible: true,
      image:true,
      pdf:false,
    });
  }
  }
  _onPressAdvice(item) {

    if(item.advice == "na.jpg")
    {
      Alert.alert(
        "Message",
        "No Advice to show",
        [
          
          { text: "OK", onPress: () => 
          {
            console.log("OK Pressed")
          

        } }
        ],
        { cancelable: false }
      );
    }else{
    this.setState({
      source: {uri: 'https://test.drtop.in/' + item.advice},
      isVisible: true,
      image:true,
      pdf:false,
    });
  }
  }

  render() {
    const {recordList,isVisible,source,pdf,image} = this.state;
    return (
      <ScrollView>
    
    <View style={styles.item}>
              <Image source = {{uri:this.props.route.params.userName.owner.avatar_url}}
   style = {{ width: 350, height: 200 }}
   />

                <Text>Full Name: {this.props.route.params.userName.full_name}</Text>
                <Text>Description :{this.props.route.params.userName.description}</Text>
                <Text>Created At : {this.props.route.params.userName.created_at}</Text>
               <Text>Updated At : {this.props.route.params.userName.updated_at}</Text>
                <Text>Pushed At  : {this.props.route.params.userName.pushed_at}</Text>
                <Text>size : {this.props.route.params.userName.size}</Text>
                <Text>default_branch :{this.props.route.params.userName.default_branch}</Text>
     <Text>open_issues_count :{this.props.route.params.userName.open_issues_count}</Text>
              </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#008080',
    padding: 10,
    marginBottom: 10,
    height: 200,
    width: 300,
  },
  button1: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    marginBottom: 10,
    height: 200,
    width: 300,
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  subtitleText: {
    fontSize: 20,
    padding: 40,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  headingtitleText: {
    fontSize: 30,
    padding: 25,
    fontWeight: 'bold',
    color: '#000000',
  },
  subheadingtitleText: {
    fontSize: 20,
    padding: 15,
    fontWeight: 'bold',
    color: '#000000',
  },
  item: {
    marginTop: 24,
    padding: 30,
    backgroundColor: 'steelblue',

    borderRadius: 10,
    fontSize: 24,
    marginEnd: 10,
    marginLeft: 10,
  },
  tinyLogo: {
    width: 50,
    height: 50,
    marginBottom: 10,
  },
  pdf: {
    flex: 1,
    width: Dimensions.get('window').width / 1.3,
    height: Dimensions.get('window').height / 1.5,
  },
});
