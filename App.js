import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './ios/Screens/HomeScreen';
import RecordsScreen from './ios/Screens/RecordsScreen';



  
  const Stack = createStackNavigator();


function App() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="RecordsScreen" component={RecordsScreen}/>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
  
  export default App;